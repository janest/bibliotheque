<?php

namespace App\Controller;

use App\Entity\Loan;
use App\Form\LoanType;
use App\Form\SearchLoanType;
use App\Repository\LoanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LoanController
 * @package App\Controller
 *
 * @Route("/emprunt")
 */
class LoanController extends AbstractController
{
    /**
     * @Route("/{page}", requirements={"page": "\d+"}, defaults={"page": 1})
     */
    public function index(
        Request $request,
        LoanRepository $loanRepository,
        $page
    ) {
//        $loans = $loanRepository->findBy(
//            [],
//            ['startDate' => 'DESC']
//        );

        $searchForm = $this->createForm(SearchLoanType::class);

        $searchForm->handleRequest($request);

        $adapter = new DoctrineORMAdapter(
            $loanRepository->searchQb((array)$searchForm->getData())
        );

        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(
            $this->getParameter('nb_loans_per_page')
        );
        $pager->setCurrentPage($page);
        return $this->render(
            'loan/index.html.twig',
            [
//                'loans' => $loans
                'pager' => $pager,
                'search_form' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/ajout")
     */
    public function add(Request $request, EntityManagerInterface $manager)
    {
        $loan = new Loan();

        $form = $this->createForm(LoanType::class, $loan);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager->persist($loan);
                $manager->flush();

                $this->addFlash('success', "L'emprunt est enregistré");

                return $this->redirectToRoute('app_loan_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'loan/add.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/retour/{id}", requirements={"id": "\d+"})
     */
    public function registerReturn(
        EntityManagerInterface $manager,
        Loan $loan
    ) {
        $loan->setEndDate(new \DateTime());

        $manager->flush();

        return $this->redirectToRoute('app_loan_index');
    }
}
