<?php

namespace App\Controller;

use App\Entity\Book;
use App\Factory\EntityFactory;
use App\Form\BookType;
use App\Form\SearchBookType;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BookController
 * @package App\Controller
 *
 * @Route("/livre")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/{page}", requirements={"page": "\d+"}, defaults={"page": 1})
     */
    public function index(
        Request $request,
        BookRepository $bookRepository,
        $page
    ) {
//        $books = $bookRepository->findBy(
//            [],
//            ['title' => 'ASC']
//        );

        $searchForm = $this->createForm(SearchBookType::class);

        $searchForm->handleRequest($request);

        $adapter = new DoctrineORMAdapter(
            $bookRepository->searchQb((array)$searchForm->getData())
        );
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(
            $this->getParameter('nb_books_per_page')
        );
        $pager->setCurrentPage($page);

        return $this->render(
            'book/index.html.twig',
            [
//                'books' => $books
                'pager' => $pager,
                'search_form' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/edit/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $manager,
        EntityFactory $factory,
        $id
    ) {
        $book = $factory->get(Book::class, $id);

        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager->persist($book);
                $manager->flush();

                $this->addFlash('success', "Le livre est enregistré");

                return $this->redirectToRoute('app_book_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'book/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(
        EntityManagerInterface $em,
        Book $book
    ) {
        $em->remove($book);
        $em->flush();

        $this->addFlash('success', "Le livre est supprimé");

        return $this->redirectToRoute('app_book_index');
    }
}
