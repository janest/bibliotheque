<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class EmployeeController
 * @package App\Controller
 *
 * @Route("/employe")

 */
class EmployeeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(EmployeeRepository $employeeRepository)
    {
        $employees = $employeeRepository->findBy(
            [],
            ['lastname' => 'ASC']
        );

        return $this->render(
            'employee/index.html.twig',
            [
                'employees' => $employees
            ]
        );
    }

    /**
     * @Route("/nouveau")
     */
    public function create(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em
    ) {
        $employee = new Employee();

        $form = $this->createForm(
            EmployeeType::class,
            $employee,
            [
                // voir groups dans NotBlank du plainPassword
                'validation_groups' => [
                    // les validations sans groupe
                    'Default',
                    // Le groupe de NotBlank du plainPassword
                    'creation'
                ]
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $password = $passwordEncoder->encodePassword(
                    $employee,
                    $employee->getPlainPassword()
                );

                $employee->setPassword($password);

                $em->persist($employee);
                $em->flush();

                $this->addFlash('success', "l'employé est créé");

                return $this->redirectToRoute('app_employee_index');
            } else {
                $this->addFlash(
                    'error',
                    'Le formulaire contient des erreurs'
                );
            }
        }

        return $this->render(
            'employee/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/edition/{id}")
     */
    public function edit(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        Employee $employee
    ) {
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (!empty($employee->getPlainPassword())) {
                    $password = $passwordEncoder->encodePassword(
                        $employee,
                        $employee->getPlainPassword()
                    );

                    $employee->setPassword($password);
                }


                $em->persist($employee);
                $em->flush();

                $this->addFlash('success', "l'employé est créé");

                return $this->redirectToRoute('app_employee_index');
            } else {
                $this->addFlash(
                    'error',
                    'Le formulaire contient des erreurs'
                );
            }
        }

        return $this->render(
            'employee/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
