<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\User;
use App\Factory\EntityFactory;
use App\Form\AuthorType;
use App\Form\UserType;
use App\Repository\AuthorRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route("/utilisateur")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(UserRepository $userRepository)
    {
        $users = $userRepository->findBy(
            [],
            ['lastname' => 'ASC']
        );

        return $this->render(
            'user/index.html.twig',
            [
                'users' => $users
            ]
        );
    }

    /**
     * @Route("/edit/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $manager,
        EntityFactory $factory,
        $id
    ) {
        $user = $factory->get(User::class, $id);

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager->persist($user);
                $manager->flush();

                $this->addFlash('success', "L'utilisateur est enregistré");

                return $this->redirectToRoute('app_user_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'author/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(
        EntityManagerInterface $em,
        User $user
    ) {
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', "L'utilisateur est supprimé");

        return $this->redirectToRoute('app_user_index');
    }
}
