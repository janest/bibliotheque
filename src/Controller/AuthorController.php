<?php

namespace App\Controller;

use App\Entity\Author;
use App\Factory\EntityFactory;
use App\Form\AuthorType;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthorController
 * @package App\Controller
 *
 * @Route("/auteur")
 */
class AuthorController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(AuthorRepository $authorRepository)
    {
        $authors = $authorRepository->findBy(
            [],
            ['lastname' => 'ASC']
        );

        return $this->render(
            'author/index.html.twig',
            [
                'authors' => $authors
            ]
        );
    }

    /**
     * @Route("/edit/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $manager,
        EntityFactory $factory,
        $id
    ) {
        $author = $factory->get(Author::class, $id);

        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager->persist($author);
                $manager->flush();

                $this->addFlash('success', "L'auteur est enregistré");

                return $this->redirectToRoute('app_author_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'author/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(
        EntityManagerInterface $em,
        Author $author
    ) {
        $em->remove($author);
        $em->flush();

        $this->addFlash('success', "L'auteur est supprimé");

        return $this->redirectToRoute('app_author_index');
    }
}
