<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class BookIsLoaned
 * @package App\Validator\Contraints
 *
 * @Annotation()
 */
class BookIsLoaned extends Constraint
{
    public $message = "Le livre '{{ titre }}' est en cours d'emprunt";
}
