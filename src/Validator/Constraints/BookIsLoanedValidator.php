<?php


namespace App\Validator\Constraints;


use App\Entity\Book;
use App\Repository\LoanRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Class BookIsLoanedValidator
 * @package App\Validator\Contraints
 */
class BookIsLoanedValidator extends ConstraintValidator
{

    /**
     * @var LoanRepository
     */
    private $loanRepository;

    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof BookIsLoaned) {
            throw new UnexpectedTypeException($constraint, BookIsLoaned::class);
        }

        if (empty($value)) {
            return;
        }

        if (!$value instanceof Book) {
            throw new UnexpectedValueException($value, Book::class);
        }

        if ($this->loanRepository->isBookLoaned($value)) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ titre }}', $value->getTitle())
                ->addViolation()
            ;

        }
    }
}
