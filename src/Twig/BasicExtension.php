<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class BasicExtension
 * @package App\Twig
 */
class BasicExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('date_fr', [$this, 'dateFr'])
        ];
    }

    public function dateFr(?\DateTime $date, $withTime = false): string
    {
        if (is_null($date)) {
            return '';
        }

        $format = 'd/m/Y';

        if ($withTime) {
            $format .= ' H:i';
        }

        return $date->format($format);
    }
}
