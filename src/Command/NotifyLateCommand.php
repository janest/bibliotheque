<?php

namespace App\Command;

use App\Repository\LoanRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Twig\Environment;

class NotifyLateCommand extends Command
{
    protected static $defaultName = 'notifyLate';

    /**
     * @var LoanRepository
     */
    private $loanRepository;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoanRepository $loanRepository,
        \Swift_Mailer $mailer,
        Environment $twig,
        LoggerInterface $logger
    ) {
        $this->loanRepository = $loanRepository;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->logger = $logger;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Notifie les emprunts en retard')
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $lateLoans = $this->loanRepository->findLateLoans();

        $io->note(count($lateLoans) . ' emprunts en retard');

        foreach ($lateLoans as $loan) {
            $mail = $this->mailer->createMessage();

            $body = $this->twig->render(
                'mail/notify_late.html.twig',
                [
                    'loan' => $loan
                ]
            );

            $mail
                ->setSubject('Emprunt en retard')
                ->setFrom('contact@bibliotheque.fr')
                ->setTo($loan->getUser()->getEmail())
                ->setBody($body, 'text/html')
            ;

            $this->mailer->send($mail);
        }
//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }

        $this->logger->info(
            'Notifications de retard : '
            . count($lateLoans) . ' mails envoyés'
        );

        $io->success('Notifications envoyées');
    }
}
