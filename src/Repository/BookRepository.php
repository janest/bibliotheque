<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function searchQb(array $filter = [])
    {
        $qb = $this->createQueryBuilder('b');

        if (!empty($filter['title'])) {
            $qb
                ->andWhere('b.title LIKE :title')
                ->setParameter('title', '%' . $filter['title'] . '%')
            ;
        }

        if (!empty($filter['author'])) {
            $qb
                ->andWhere(':author MEMBER OF b.authors')
                ->setParameter('author', $filter['author'])
            ;
        }

        $qb->orderBy('b.title');

        return $qb;
    }
}
