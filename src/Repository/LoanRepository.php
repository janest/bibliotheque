<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Loan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Loan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Loan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Loan[]    findAll()
 * @method Loan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoanRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Loan::class);
    }

    public function isBookLoaned(Book $book): bool
    {
        $qb = $this->createQueryBuilder('l');

        $qb
            ->select('count(l)')
            ->andWhere('l.endDate IS NULL')
            ->andWhere('l.book = :book')
            ->setParameter('book', $book)
        ;

        $nb = $qb->getQuery()->getSingleScalarResult();

        return $nb != 0;
    }

    public function searchQb(array $filters = [])
    {
        $qb = $this->createQueryBuilder('l');

        if (!empty($filters['current']) || !empty($filters['late'])) {
            $qb->andWhere('l.endDate IS NULL');
        }

        if (!empty($filters['late'])) {
            $now = new \DateTime();
            $dateLimit = $now->sub(Loan::getReturnDelay());

            $qb
                ->andWhere('l.startDate < :dateLimit')
                ->setParameter('dateLimit', $dateLimit)
            ;
        }


        $qb->orderBy('l.startDate', 'DESC');

        return $qb;
    }

    public function findLateLoans()
    {
        $now = new \DateTime();
        $dateLimit = $now->sub(Loan::getReturnDelay());

        $qb = $this->createQueryBuilder('l');

        $qb
            ->andWhere('l.endDate IS NULL')
            ->andWhere('l.startDate < :dateLimit')
            ->setParameter('dateLimit', $dateLimit)
        ;

        return $qb->getQuery()->getResult();
    }
}
