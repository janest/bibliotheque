<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Loan;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'book',
                EntityType::class,
                [
                    'label' => 'Livre',
                    'class' => Book::class,
                    'placeholder' => 'Choisissez'
                ]
            )
            ->add(
                'user',
                EntityType::class,
                [
                    'label' => 'Utilisateur',
                    'class' => User::class,
                    'placeholder' => 'Choisissez'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Loan::class,
        ]);
    }
}
