<?php
namespace App\Factory;

use Doctrine\ORM\EntityManagerInterface;


/**
 * Class EntityFactory
 */
class EntityFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function get($class, $id = null)
    {
        if (is_null($id)) {
            return new $class;
        }

        else {
            $repository = $this->entityManager->getRepository($class);

            return $repository->find($id);
        }
    }
}
